import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainReader {

        /**
         * Формат даты
         */
        private static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";

        /**
         * Форматтер, используется для преобразования строк в даты и обратно
         */
        private static SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);


        /**
         * Точка входа в программу
         * @param args
         */
        public static void main(String[] args){

            try {
                readFile();
            } catch (FileNotFound e) {
                e.getStackTrace();
            }
        }
        /**
         * Метод для чтения дат из файла
         */
        public static void readFile() throws FileNotFound {
            //Открываем потоки на чтение из файла
          //  FileReader reader = null;
            try (FileReader reader = new FileReader("D:\\file.txt");
                 BufferedReader byfReader = new BufferedReader(reader)) {


           // BufferedReader byfReader = new BufferedReader(reader);


            //Читаем первую строку из файла
            String strDate = null;
            try {
                strDate = byfReader.readLine();
            } catch (IOException e) {
                System.out.println("Файл не был найден!!!");
            }

            while(strDate != null) {
                //Преобразуем строку в дату
                Date date = null;
                try {
                    date = parseDate(strDate);
                } catch (ParseException e) {
                    System.out.println("Файле содержится некорреткная дата: " + strDate);
                   // e.printStackTrace();

                }
                if (date != null)
                //Выводим дату в консоль в формате dd-mm-yy
                System.out.printf("%1$td-%1$tm-%1$ty \n", date);

                //Читаем следующую строку из файла
                strDate = byfReader.readLine();
            }
            } catch (FileNotFoundException e) {
                System.out.println("Файл не был найден!!!");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * Метод преобразует строковое представление даты в класс Date
         * @param strDate строковое представление даты
         * @return
         */
        public static Date parseDate(String strDate) throws ParseException {
                return dateFormatter.parse(strDate);
            }
    }

