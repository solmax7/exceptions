public class FileNotFound extends Exception{

    public FileNotFound(Throwable e) {
        super(e);
    }

}
